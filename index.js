let trainer = {
		name : "Ash Ketchum",
		age : 10,
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		talk: function(){
			console.log(this.pokemon[0] + "!" + "I choose you!");
		}
	}

	console.log(trainer);
	console.log("Result of dot notation : ");
	console.log(trainer.name);
	console.log("Result of sqaure bracket notation : ");
	console.log(trainer["pokemon"]);
	console.log("Result talk method");
	trainer.talk();

	function Pokemon(name, level){
		
		this.name = name;
		this.level = level;
		this.health = level*2;
		this.attack = level;
		this.tackle = function(target){
			console.log(this.name + 'tackled ' + target.name);
			target.health -= this.attack;
			console.log(target.name + "'s" + " health is now reduced to "+ target.health);

			if(target.health <= 0){ 
			    target.faint()
			}

		}
		this.faint = function(){
			console.log(this.name + " fainted");
		}
	}

	let pikachu = new Pokemon("Pikachu ", 12);
	console.log(pikachu);

	let geoDude = new Pokemon("Geodude ", 8);
	console.log(geoDude);

	let mewTwo = new Pokemon("MewTwo ", 100);
	console.log(mewTwo);
	geoDude.tackle(pikachu);

	console.log(pikachu);

	mewTwo.tackle(geoDude);
	console.log(geoDude);
